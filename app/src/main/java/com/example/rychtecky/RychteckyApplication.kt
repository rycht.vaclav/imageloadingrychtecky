package com.example.rychtecky

import android.app.Application
import com.example.app_data.di.appModule
import com.example.rychtecky.di.viewModelModule
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin

class RychteckyApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

    private fun initKoin() {

        startKoin {
            androidContext(this@RychteckyApplication)
            modules(
                appModule + viewModelModule
            )
        }
    }

    inline fun <reified T : Any> getKoinInstance(): T {
        return object : KoinComponent {
            val value: T by inject()
        }.value
    }
}