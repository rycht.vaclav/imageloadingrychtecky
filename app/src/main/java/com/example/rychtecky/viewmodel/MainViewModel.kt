package com.example.rychtecky.viewmodel

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app_domain.feature.GetImageStringUseCase
import com.example.rychtecky.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainViewModel(val getImageString: GetImageStringUseCase) : ViewModel() {

    private val _bitmap = MutableStateFlow<Bitmap?>(null)
    val bitmap = _bitmap.asStateFlow()

    private val _loading = MutableStateFlow(false)
    val loading = _loading.asStateFlow()

    private val _error = MutableStateFlow<Int?>(null)
    val error = _error.asStateFlow()

    fun getImageString(userName: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                cleanState()
                _loading.emit(true)
                getImageString(GetImageStringUseCase.Params(userName, password)).getOrNull()?.let {
                    getImageFromBase64String(it.base64Image)
                } ?: onError()
            } catch (e: java.lang.Exception){
                onError()
            }
        }
    }

    private suspend fun getImageFromBase64String(base64Image: String) {
        val decodedString = Base64.decode(base64Image, Base64.DEFAULT)
        _bitmap.emit(BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size))
        _loading.emit(false)
    }

    private suspend fun onError() {
        _error.emit(R.string.error)
        _loading.emit(false)
    }

    private suspend fun cleanState() {
        _bitmap.emit(null)
        _error.emit(null)
    }
}