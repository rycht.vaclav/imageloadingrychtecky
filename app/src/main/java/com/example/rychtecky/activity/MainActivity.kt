package com.example.rychtecky.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.rychtecky.ui.layout.LoginScaffold
import com.example.rychtecky.ui.theme.RychteckyTheme
import com.example.rychtecky.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : ComponentActivity() {

    private val viewModel : MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RychteckyTheme {
                LoginScaffold(viewModel = viewModel)
            }
        }
    }
}