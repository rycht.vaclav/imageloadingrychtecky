package com.example.rychtecky.ui.layout

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.example.rychtecky.R
import com.example.rychtecky.ui.theme.defaultPadding
import com.example.rychtecky.viewmodel.MainViewModel

@Composable
fun LoginScaffold(viewModel: MainViewModel) {
    Scaffold(
        topBar = {
            TopAppBar {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = stringResource(id = R.string.app_name)
                    )
                }
            }
        }
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = MaterialTheme.colors.background
        ) {
            LoginScreen(viewModel)
        }
    }
}

@Composable
fun LoginScreen(
    viewModel: MainViewModel,
) {
    Column(
        modifier = Modifier.padding(horizontal = defaultPadding),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        val bitmap by viewModel.bitmap.collectAsState()
        val loading by viewModel.loading.collectAsState()
        val error by viewModel.error.collectAsState()

        var userName by remember { mutableStateOf("") }
        var password by remember { mutableStateOf("") }

        Spacer(modifier = Modifier.height(defaultPadding))
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = userName,
            onValueChange = { userName = it },
            label = { Text(text = stringResource(id = R.string.user_name)) },
        )
        Spacer(modifier = Modifier.height(defaultPadding))
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = password,
            onValueChange = { password = it },
            label = { Text(text = stringResource(id = R.string.password)) },
        )
        Spacer(modifier = Modifier.height(defaultPadding))
        Button(onClick = { viewModel.getImageString(userName, password) }) {
            Text(text = stringResource(id = R.string.login))
        }
        Box(modifier = Modifier.fillMaxSize(1f), contentAlignment = Alignment.Center) {
            bitmap?.let {
                Image(
                    modifier = Modifier.fillMaxSize(),
                    bitmap = it.asImageBitmap(),
                    contentDescription = ""
                )
            }
            if (loading) {
                CircularProgressIndicator()
            }
            error?.let {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = stringResource(id = it),
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}