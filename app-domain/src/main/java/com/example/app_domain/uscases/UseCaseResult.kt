package com.archonalabs.mrkev.domain.usecases

import com.example.app_domain.Result

abstract class UseCaseResult<out T : Any, in Params> : UseCase<Result<T>, Params>()