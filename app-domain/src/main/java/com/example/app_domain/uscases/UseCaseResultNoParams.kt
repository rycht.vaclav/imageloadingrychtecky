package com.archonalabs.mrkev.domain.usecases

import com.example.app_domain.Result

abstract class UseCaseResultNoParams<out T : Any> : UseCase<Result<T>, Unit>() {
    suspend operator fun invoke() = super.invoke(Unit)
}