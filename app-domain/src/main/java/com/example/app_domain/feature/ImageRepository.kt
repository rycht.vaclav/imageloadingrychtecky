package com.example.app_domain.feature

import com.example.app_domain.Result
import com.example.app_domain.model.ImageResult

interface ImageRepository {
    suspend fun getImageString(userName: String, password: String): Result<ImageResult>
}