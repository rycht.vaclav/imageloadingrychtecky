package com.example.app_domain.feature

import com.archonalabs.mrkev.domain.usecases.UseCaseResult
import com.example.app_domain.model.ImageResult
import com.example.app_domain.Result

class GetImageStringUseCase(private val imageRepository: ImageRepository) :
    UseCaseResult<ImageResult, GetImageStringUseCase.Params>() {

    override suspend fun doWork(params: Params): Result<ImageResult> {
        return imageRepository.getImageString(params.userName, params.password)
    }

    data class Params(val userName: String, val password: String)
}