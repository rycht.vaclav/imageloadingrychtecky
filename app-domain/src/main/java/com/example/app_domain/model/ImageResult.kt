package com.example.app_domain.model

data class ImageResult(
    val base64Image: String
)