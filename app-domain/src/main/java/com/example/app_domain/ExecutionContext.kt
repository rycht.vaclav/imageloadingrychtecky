package com.example.app_domain

sealed class Result<out T : Any> {

    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Running<out T : Any>(val data: T? = null) : Result<T>()
    data class Error<out T : Any>(val error: ErrorResult, val data: T? = null) : Result<T>()

    fun isFinished() = this is Success || this is Error

    fun isRunning() = this is Running

    fun isError() = this is Error

    fun errorOrNull() = when {
        this is Error -> error
        else -> null
    }

    fun isSuccess() = this is Success

    fun getOrNull() = when {
        this is Success -> data
        this is Running -> data
        this is Error -> data
        else -> null
    }
}

open class ErrorResult(open var message: String? = null, open var throwable: Throwable? = null)
