package com.example.app_data

import com.example.app_domain.ErrorResult
import com.example.app_domain.Result
import com.example.app_domain.feature.ImageSource
import com.example.app_domain.model.ImageResult
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.security.MessageDigest

class ImageSourceImpl() : ImageSource {

    override suspend fun getImageString(userName: String, password: String): Result<ImageResult> {
        return getImage(userName, password)
    }

    private suspend fun getImage(userName: String, password: String): Result<ImageResult> {

        val body = FormBody.Builder()
            .add(name = "username", value = password)
            .build()
        val request = Request.Builder()
            .url("https://mobility.cleverlance.com/download/bootcamp/image.php")
            .addHeader(name = "authorization", value = hashPassword(userName))
            .post(body)
            .build()


        val call = OkHttpClient().newCall(request).execute()

        return if (call.isSuccessful) {
            Result.Success(
                data = ImageResult(
                    base64Image = JSONObject(call.body?.string().toString()).get("image").toString()
                )
            )
        } else {
            Result.Error(
                error = ErrorResult(message = call.code.toString())
            )
        }
    }

    private fun hashPassword(password: String) = MessageDigest
        .getInstance("SHA-1")
        .digest(password.toByteArray())
        .joinToString("") { "%02x".format(it) }

}