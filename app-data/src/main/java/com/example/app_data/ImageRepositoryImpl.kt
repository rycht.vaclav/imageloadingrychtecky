package com.example.app_data

import com.example.app_domain.feature.ImageRepository
import com.example.app_domain.feature.ImageSource
import com.example.app_domain.Result
import com.example.app_domain.model.ImageResult

class ImageRepositoryImpl(private val imageSource: ImageSource): ImageRepository {

    override suspend fun getImageString(userName: String, password: String): Result<ImageResult> {
        return imageSource.getImageString(userName, password)
    }
}