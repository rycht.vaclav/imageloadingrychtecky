package com.example.app_data.di

import com.example.app_data.ImageRepositoryImpl
import com.example.app_data.ImageSourceImpl
import com.example.app_domain.feature.GetImageStringUseCase
import com.example.app_domain.feature.ImageRepository
import com.example.app_domain.feature.ImageSource
import org.koin.dsl.module

val appModule = module {

    single { GetImageStringUseCase(get()) }

    factory<ImageSource> { ImageSourceImpl() }

    factory<ImageRepository> { ImageRepositoryImpl(get()) }
}